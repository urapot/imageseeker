package com.urapot.imageseeker;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by urapot on 27.07.2015.
 */
public class ImageListAdapter extends ArrayAdapter<ImageInfo> implements View.OnClickListener {
    private static final String TAG = "ImageListAdapter";

    private Context context;
    private List<ImageInfo> imageList;
    private ImageLoader imageLoader;

    public ImageListAdapter(Context context, List<ImageInfo> objects) {
        super(context, R.layout.images_listview_item, objects);
        this.context = context;
        this.imageList = objects;
        this.imageLoader = new ImageLoader(getContext());
    }

    static class ViewHolder{
        protected ImageView ivThumb;
        protected TextView twTitle;
        protected CheckBox cbFavorite;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup parent) {
        View itemView = null;
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            itemView = inflater.inflate(R.layout.images_listview_item, parent, false);
            final ViewHolder viewHolder = new ViewHolder();
            viewHolder.ivThumb = (ImageView) itemView.findViewById(R.id.item_image);
            viewHolder.twTitle = (TextView) itemView.findViewById(R.id.item_title);
            viewHolder.cbFavorite = (CheckBox) itemView.findViewById(R.id.item_check);
            viewHolder.cbFavorite.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CompoundButton buttonView = (CompoundButton)v;
                    ImageInfo ii = (ImageInfo) viewHolder.cbFavorite.getTag();
                    ii.setFavorite(buttonView.isChecked());
                    if (buttonView.isChecked()) {
                        //add to favorite
                        Log.d(TAG, "Add to favorites: " + ii.getTitle());
                        Intent intent = new Intent(MainActivity.INTERACT_WITH_DB);
                        intent.putExtra(MainActivity.INTERACT_DB_IMAGE_EXTRA, ii.toIIString());
                        intent.putExtra(MainActivity.INTERACT_DB_FUNCTION, MainActivity.INTERACT_DB_ADD);
                        context.sendBroadcast(intent);
                    } else {
                        //remove from favorite
                        Log.d(TAG, "Delete from favorites: " + ii.getTitle());
                        Intent intent = new Intent(MainActivity.INTERACT_WITH_DB);
                        intent.putExtra(MainActivity.INTERACT_DB_IMAGE_EXTRA, ii.toIIString());
                        intent.putExtra(MainActivity.INTERACT_DB_FUNCTION, MainActivity.INTERACT_DB_DELETE);
                        context.sendBroadcast(intent);
                    }
                }
            });
/*            viewHolder.cbFavorite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ImageInfo ii = (ImageInfo) viewHolder.cbFavorite.getTag();
                    ii.setFavorite(buttonView.isChecked());
                    if(buttonView.isChecked()){
                        //add to favorite
                        Log.d(TAG, "Add to favorites: " + ii.getTitle());
                        Intent intent = new Intent(MainActivity.INTERACT_WITH_DB);
                        intent.putExtra(MainActivity.INTERACT_DB_IMAGE_EXTRA, ii.toIIString());
                        intent.putExtra(MainActivity.INTERACT_DB_FUNCTION, MainActivity.INTERACT_DB_ADD);
                        context.sendBroadcast(intent);
                    }
                    else {
                        //remove from favorite
                        Log.d(TAG, "Delete from favorites: " + ii.getTitle());
                        Intent intent = new Intent(MainActivity.INTERACT_WITH_DB);
                        intent.putExtra(MainActivity.INTERACT_DB_IMAGE_EXTRA, ii.toIIString());
                        intent.putExtra(MainActivity.INTERACT_DB_FUNCTION, MainActivity.INTERACT_DB_ADD);
                        context.sendBroadcast(intent);
                    }
                }
            });*/
            itemView.setTag(viewHolder);
            viewHolder.cbFavorite.setTag(imageList.get(position));
        }
        else {
            itemView = convertView;
            ((ViewHolder)itemView.getTag()).cbFavorite.setTag(imageList.get(position));
        }

        ViewHolder viewHolder = (ViewHolder)itemView.getTag();
        viewHolder.twTitle.setText(imageList.get(position).getTitle());
        viewHolder.cbFavorite.setChecked(imageList.get(position).isFavorite());
        viewHolder.ivThumb.setTag(imageList.get(position));
        if(imageList.get(position).isFavorite()) {
            imageLoader.DisplayImage(imageList.get(position).getThumbnailLink(), viewHolder.ivThumb, ImageLoader.SHOW_FROM_DB);
        }
        else {
            imageLoader.DisplayImage(imageList.get(position).getThumbnailLink(), viewHolder.ivThumb, ImageLoader.SHOW_FROM_SEARCH);
        }
        viewHolder.ivThumb.setOnClickListener(this);

        return itemView;
    }

    @Override
    public void onClick(View v) {
        ImageInfo imageInfo = (ImageInfo)v.getTag();
        Log.d(TAG, "image clicked:" + imageInfo.getTitle() + " " + imageInfo.getLink());
        Intent intent = new Intent(MainActivity.START_BIG_IMAGE_FRAGMENT);
        intent.putExtra(MainActivity.BIG_IMAGE_URL_EXTRA, imageInfo.getLink());
        intent.putExtra(MainActivity.BIG_IMAGE_TITLE_EXTRA, imageInfo.getTitle());
        intent.putExtra(MainActivity.BIG_IMAGE_FAVORITE_EXTRA, imageInfo.isFavorite());
        context.sendBroadcast(intent);
    }
}
