package com.urapot.imageseeker;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by urapot on 29.07.2015.
 */
public class DBAdapter {
    private static final String DB_NAME = "com.urapot.imageseeker.images";
    private static final int DB_VERSION = 1;
    private static final String DB_FAVORITES_IMAGES_TABLE = "favorites";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_LINK = "link";
    public static final String COLUMN_HEIGHT = "height";
    public static final String COLUMN_WIDTH = "width";
    public static final String COLUMN_SIZE = "byteSize";
    public static final String COLUMN_THUMBLINK = "thumbLink";
    public static final String COLUMN_THUMBHEIGHT = "thumbHeight";
    public static final String COLUMN_THUMBWIDTH = "thumbWidth";

    private Context context;
    private DBHelper dbHelper;
    private SQLiteDatabase sqliteDB;

    public DBAdapter(Context context) {
        this.context = context;
    }

    public void open(){
        dbHelper = new DBHelper(context, DB_NAME, null, DB_VERSION);
        sqliteDB = dbHelper.getWritableDatabase();
    }

    public void close(){
        if(dbHelper != null){
            dbHelper.close();
        }
    }

    public Cursor getAllData(){
        return sqliteDB.query(DB_FAVORITES_IMAGES_TABLE, null, null, null, null, null, null);
    }

    public Cursor getTenRecord(int firstId){
        return sqliteDB.query(DB_FAVORITES_IMAGES_TABLE, null, COLUMN_ID + " > ?", new String[] {((Integer)firstId).toString()}, null, null, null, "10");
    }

    public void addRecord(ImageInfo imageInfo){
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_TITLE, imageInfo.getTitle());
        cv.put(COLUMN_LINK, imageInfo.getLink());
        cv.put(COLUMN_HEIGHT, imageInfo.getHeight());
        cv.put(COLUMN_WIDTH, imageInfo.getWidth());
        cv.put(COLUMN_SIZE, imageInfo.getByteSize());
        cv.put(COLUMN_THUMBLINK, imageInfo.getThumbnailLink());
        cv.put(COLUMN_THUMBHEIGHT, imageInfo.getThumbnailHeight());
        cv.put(COLUMN_THUMBWIDTH, imageInfo.getThumbnailWidth());
        sqliteDB.insert(DB_FAVORITES_IMAGES_TABLE, null, cv);
    }

    public void delRecord(String link){
        sqliteDB.delete(DB_FAVORITES_IMAGES_TABLE, COLUMN_LINK + " = '" + link + "'", null);
    }

    public Cursor findRecords(String link){
        return sqliteDB.query(DB_FAVORITES_IMAGES_TABLE, null, COLUMN_LINK + " = ?", new String[]{link}, null, null, null);
    }

    private class DBHelper extends SQLiteOpenHelper{

        public DBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            String sqlCreate = "create table " + DB_FAVORITES_IMAGES_TABLE + "(" +
                    COLUMN_ID + " integer primary key autoincrement, " +
                    COLUMN_TITLE + " text, " +
                    COLUMN_LINK + " text, " +
                    COLUMN_HEIGHT + " integer, " +
                    COLUMN_WIDTH + " integer, " +
                    COLUMN_SIZE + " integer, " +
                    COLUMN_THUMBLINK + " text, " +
                    COLUMN_THUMBHEIGHT + " integer, " +
                    COLUMN_THUMBWIDTH + " integer" +
                    ");";
            db.execSQL(sqlCreate);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
