package com.urapot.imageseeker;


import android.app.Dialog;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

public class BigImageFragment extends DialogFragment {
    private static final String IMAGE_URL_ARG = "image_url_argument";
    private static final String IMAGE_TITLE_ARG = "image_title_argument";
    private static final String IMAGE_FAVORITE_ARG = "image_favorite_argument";

    private String imageURL;
    private String imageTitle;
    private boolean favorite;
    private ImageLoader imageLoader;


    public static BigImageFragment newInstance(String url, String title, boolean favorite) {
        BigImageFragment fragment = new BigImageFragment();
        Bundle args = new Bundle();
        args.putString(IMAGE_URL_ARG, url);
        args.putString(IMAGE_TITLE_ARG, title);
        args.putBoolean(IMAGE_FAVORITE_ARG, favorite);
        fragment.setArguments(args);
        return fragment;
    }

    public BigImageFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageLoader = new ImageLoader(getActivity());
        if (getArguments() != null) {
            imageURL = getArguments().getString(IMAGE_URL_ARG);
            imageTitle = getArguments().getString(IMAGE_TITLE_ARG);
            favorite = getArguments().getBoolean(IMAGE_FAVORITE_ARG);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_big_image, container, false);

        ImageView ivBig = (ImageView)rootView.findViewById(R.id.big_image_view);
        ivBig.setTag(new ImageInfo(imageURL, imageURL));
        if(favorite){
            imageLoader.DisplayImage(imageURL, ivBig, ImageLoader.SHOW_FROM_DB);
        }
        else {
            imageLoader.DisplayImage(imageURL, ivBig, ImageLoader.SHOW_FROM_SEARCH);
        }

        return rootView;
    }


}
