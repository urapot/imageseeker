package com.urapot.imageseeker;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

/**
 * Created by urapot on 27.07.2015.
 */
public class MyFragmentPagerAdapter extends FragmentPagerAdapter {

    public MyFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0:
                title = "Search result";
                break;

            case 1:
                title = "Favorites";
                break;
        }
        return title;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 0:
                //search result
                fragment = ImageListFragment.newInstance(MainActivity.IMAGES_SEARCH_RESULT);
                break;

            case 1:
                //favorites
                fragment = ImageListFragment.newInstance(MainActivity.IMAGES_FAVORITE);
                break;
        }
        return fragment;
    }

    @Override
    public int getItemPosition(Object object) {
        Log.d("PageAdapter", "getItemPosition");
        ImageListFragment fragment = (ImageListFragment)object;
        if(fragment != null){
            fragment.update();
        }
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return 2;
    }
}
