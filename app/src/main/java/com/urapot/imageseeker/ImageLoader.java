package com.urapot.imageseeker;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Stack;

/**
 * Created by urapot on 29.07.2015.
 */
public class ImageLoader {
    public static final String LOAD_FOR_SHOW = "load_for_show";
    public static final String LOAD_FOR_DB = "load_for_db";

    public static final String SHOW_FROM_SEARCH = "show_from_search";
    public static final String SHOW_FROM_DB = "show_from_db";

    private HashMap<String, Bitmap> cache = new HashMap<String, Bitmap>();
    private HashMap<String, Bitmap> dbcashe = new HashMap<String, Bitmap>();
    private File cacheDir;
    private File dbImagesDir;
    Context context;

    public ImageLoader(Context context)
    {
        this.context = context;

        photoLoaderThread.setPriority(Thread.NORM_PRIORITY-1);

        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),context.getString(R.string.cashe_dirname));
        else
            cacheDir=context.getCacheDir();

        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
            dbImagesDir=new File(android.os.Environment.getExternalStorageDirectory(),context.getString(R.string.dbimages_dirname));
        else
            dbImagesDir=context.getCacheDir();


        if(!cacheDir.exists())
            cacheDir.mkdirs();

        if(!dbImagesDir.exists()){
            dbImagesDir.mkdirs();
        }
    }

    final int stub_id=R.drawable.abs__ic_search;

    public void DisplayImage(String url, ImageView imageView, String mode)
    {
        HashMap<String, Bitmap> source = new HashMap<String, Bitmap>();
        switch (mode){
            case SHOW_FROM_SEARCH:
                source = cache;
                break;

            case SHOW_FROM_DB:
                source = dbcashe;
                break;

        }
        if(source.containsKey(url))
            imageView.setImageBitmap(source.get(url));
        else
        {
            queuePhoto(url, imageView, LOAD_FOR_SHOW);
            imageView.setImageResource(stub_id);
        }
    }

    public void SaveImage(String url){
        queuePhoto(url, new ImageView(null), LOAD_FOR_DB);
    }

    private void queuePhoto(String url, ImageView imageView, String mode)
    {
        photosQueue.Clean(imageView);
        PhotoToLoad p=new PhotoToLoad(url, imageView, mode);
        synchronized(photosQueue.photosToLoad){
            photosQueue.photosToLoad.push(p);
            photosQueue.photosToLoad.notifyAll();
        }

        if(photoLoaderThread.getState()==Thread.State.NEW)
            photoLoaderThread.start();
    }

    private Bitmap getBitmap(String url, String mode)
    {
        String filename=String.valueOf(url.hashCode());
        File f = null;
        switch (mode){
            case LOAD_FOR_SHOW:
                f = new File(cacheDir, filename);
                break;

            case LOAD_FOR_DB:
                f = new File(dbImagesDir, filename);
                break;
        }

        Bitmap b = decodeFile(f);
        if(b!=null)
            return b;

        try {
            Bitmap bitmap=null;
            InputStream is=new URL(url).openStream();
            OutputStream os = new FileOutputStream(f);
            Utils.CopyStream(is, os);
            os.close();
            bitmap = decodeFile(f);
            return bitmap;
        }
        catch(MalformedURLException e)
        {
            Bitmap bookDefaultIcon = BitmapFactory.decodeResource(context.getResources(),
                    R.drawable.abs__ic_search);
            return bookDefaultIcon;
        }
        catch (Exception ex){
            ex.printStackTrace();
            return null;
        }
    }

    private Bitmap decodeFile(File f){
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            final int REQUIRED_SIZE=70;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale++;
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=1;//scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }

    private class PhotoToLoad
    {
        public String url;
        public ImageView imageView;
        public String mode;
        public PhotoToLoad(String u, ImageView i, String mode){
            url=u;
            imageView=i;
            this.mode = mode;
        }
    }

    PhotosQueue photosQueue=new PhotosQueue();

    public void stopThread()
    {
        photoLoaderThread.interrupt();
    }

    class PhotosQueue
    {
        private Stack<PhotoToLoad> photosToLoad=new Stack<PhotoToLoad>();

        public void Clean(ImageView image)
        {
            for(int j=0 ;j<photosToLoad.size();){
                if(photosToLoad.get(j).imageView==image)
                    photosToLoad.remove(j);
                else
                    ++j;
            }
        }
    }

    class PhotosLoader extends Thread {
        public void run() {
            try {
                while(true)
                {
                    if(photosQueue.photosToLoad.size()==0)
                        synchronized(photosQueue.photosToLoad){
                            photosQueue.photosToLoad.wait();
                        }
                    if(photosQueue.photosToLoad.size()!=0)
                    {
                        PhotoToLoad photoToLoad;
                        synchronized(photosQueue.photosToLoad){
                            photoToLoad=photosQueue.photosToLoad.pop();
                        }
                        Bitmap bmp=getBitmap(photoToLoad.url, photoToLoad.mode);
                        switch (photoToLoad.mode){
                            case LOAD_FOR_SHOW:
                                cache.put(photoToLoad.url, bmp);
                                if(((((ImageInfo)photoToLoad.imageView.getTag()).getThumbnailLink()).equals(photoToLoad.url))
                                        || ((((ImageInfo)photoToLoad.imageView.getTag()).getLink()).equals(photoToLoad.url))){
                                    BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad.imageView);
                                    photoToLoad.imageView.post(bd);
                                }
                                break;

                            case LOAD_FOR_DB:
                                dbcashe.put(photoToLoad.url, bmp);
                                break;
                        }
                    }
                    if(Thread.interrupted())
                        break;
                }
            } catch (InterruptedException e) {
            }
        }
    }

    PhotosLoader photoLoaderThread=new PhotosLoader();

    class BitmapDisplayer implements Runnable
    {
        Bitmap bitmap;
        ImageView imageView;
        public BitmapDisplayer(Bitmap b, ImageView i){bitmap=b;imageView=i;}
        public void run()
        {
            if(bitmap!=null)
                imageView.setImageBitmap(bitmap);
            else
                imageView.setImageResource(stub_id);
        }
    }

    public void clearCache() {
        cache.clear();

        File[] files=cacheDir.listFiles();
        for(File f:files)
            f.delete();
    }


}
