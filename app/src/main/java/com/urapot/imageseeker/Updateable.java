package com.urapot.imageseeker;

import java.util.ArrayList;

/**
 * Created by urapot on 28.07.2015.
 */
public interface Updateable {
    public void update();
}
