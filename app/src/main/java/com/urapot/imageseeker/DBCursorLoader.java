package com.urapot.imageseeker;

import android.content.Context;
import android.support.v4.content.CursorLoader;
import android.database.Cursor;
import android.os.Bundle;

/**
 * Created by urapot on 30.07.2015.
 */
public class DBCursorLoader extends CursorLoader {
    public static final String MODE_ARG = "mode_argument";
    public static final String MODE_GET_TEN = "get_ten_records_mode";
    public static final String FIRST_ID_ARG = "first_id_argument";
    public static final String MODE_FIND_RECORDS = "find_records_mode";

    private DBAdapter db;
    private int firstId;
    private String mode;

    public DBCursorLoader(Context context, DBAdapter db, Bundle args) {
        super(context);
        if(args != null){
            mode = args.getString(MODE_ARG);
            switch (mode){
                case MODE_GET_TEN:
                    firstId = args.getInt(FIRST_ID_ARG);
                    break;

                case MODE_FIND_RECORDS:
                    break;
            }
        }
        this.db = db;
    }

    @Override
    public Cursor loadInBackground() {
        Cursor cursor = null;
        switch (mode){
            case MODE_GET_TEN:
                cursor = db.getAllData();
                break;

            case MODE_FIND_RECORDS:
                break;
        }
        return cursor;
    }
}
