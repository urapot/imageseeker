package com.urapot.imageseeker;

import android.util.Log;

import java.util.ArrayList;

/**
 * Created by urapot on 27.07.2015.
 */
public class ImageInfo {
    private String title;
    private String link;
    private int height;
    private int width;
    private int byteSize;
    private String thumbnailLink;
    private int thumbnailHeight;
    private int thumbnailWidth;
    private boolean favorite;

    public ImageInfo() {
    }

    public ImageInfo(String link, String thumbnailLink) {
        this.link = link;
        this.thumbnailLink = thumbnailLink;
    }

    public ImageInfo(String title, String link, int height, int width, int byteSize, String thumbnailLink, int thumbnailHeight, int thumbnailWidth, boolean favorite) {
        this.title = title;
        this.link = link;
        this.height = height;
        this.width = width;
        this.byteSize = byteSize;
        this.thumbnailLink = thumbnailLink;
        this.thumbnailHeight = thumbnailHeight;
        this.thumbnailWidth = thumbnailWidth;
        this.favorite = favorite;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getByteSize() {
        return byteSize;
    }

    public void setByteSize(int byteSize) {
        this.byteSize = byteSize;
    }

    public String getThumbnailLink() {
        return thumbnailLink;
    }

    public void setThumbnailLink(String thumbnailLink) {
        this.thumbnailLink = thumbnailLink;
    }

    public int getThumbnailHeight() {
        return thumbnailHeight;
    }

    public void setThumbnailHeight(int thumbnailHeight) {
        this.thumbnailHeight = thumbnailHeight;
    }

    public int getThumbnailWidth() {
        return thumbnailWidth;
    }

    public void setThumbnailWidth(int thumbnailWidth) {
        this.thumbnailWidth = thumbnailWidth;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public String toString() {
        return "ImageInfo{" +
                "title='" + title + '\'' +
                ", link='" + link + '\'' +
                ", height=" + height +
                ", width=" + width +
                ", byteSize=" + byteSize +
                ", thumbnailLink='" + thumbnailLink + '\'' +
                ", thumbnailHeight=" + thumbnailHeight +
                ", thumbnailWidth=" + thumbnailWidth +
                ", favorite=" + favorite +
                '}';
    }

    public ArrayList<String> toIIString(){
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add(getTitle());//0
        arrayList.add(getLink());//1
        arrayList.add(((Integer) getHeight()).toString());//2
        arrayList.add(((Integer) getWidth()).toString());//3
        arrayList.add(((Integer) getByteSize()).toString());//4
        arrayList.add(getThumbnailLink());//5
        arrayList.add(((Integer) getThumbnailHeight()).toString());//6
        arrayList.add(((Integer) getThumbnailWidth()).toString());//7
        arrayList.add(((Boolean)isFavorite()).toString());//8
        return arrayList;
    }

    public static ImageInfo fromIIString(ArrayList<String> arrayList){
        ImageInfo imageInfo = new ImageInfo();
        imageInfo.setTitle(arrayList.get(0));
        imageInfo.setLink(arrayList.get(1));
        imageInfo.setWidth(Integer.parseInt(arrayList.get(2)));
        imageInfo.setHeight(Integer.parseInt(arrayList.get(3)));
        imageInfo.setByteSize(Integer.parseInt(arrayList.get(4)));
        imageInfo.setThumbnailLink(arrayList.get(5));
        imageInfo.setThumbnailHeight(Integer.parseInt(arrayList.get(6)));
        imageInfo.setThumbnailWidth(Integer.parseInt(arrayList.get(7)));
        imageInfo.setFavorite(Boolean.parseBoolean(arrayList.get(8)));
        return imageInfo;
    }
}
