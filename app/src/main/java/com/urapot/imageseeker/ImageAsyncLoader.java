package com.urapot.imageseeker;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonFactory;
import com.google.api.client.googleapis.GoogleUtils;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.model.Result;
import com.google.api.services.customsearch.model.Search;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by urapot on 28.07.2015.
 */
public class ImageAsyncLoader extends AsyncTaskLoader {
    private static final String TAG = "ImageAsyncLoader";

    public static final String QUERY_ARG = "search_query_string";
    public static final String SEARCH_POSITION_ARG = "search_position"; //first or next search
    public static final String NEXT_SEARCH_ARGS = "next_search_arguments";

    public static final String FIRST_SEARCH = "search_position_first";
    public static final String NEXT_SEARCH = "search_position_next";

    String query;
    boolean firstSearch;
    int startIndex;
    JSONObject nextSearchArgs;
    JSONObject json;

    public ImageAsyncLoader(Context context, Bundle args) {
        super(context);
    }

    public void  setArgs(Bundle args){
        if(args != null){
            query = args.getString(QUERY_ARG);
            Log.d(TAG,args.getString(SEARCH_POSITION_ARG));
            switch (args.getString(SEARCH_POSITION_ARG)){
                case FIRST_SEARCH:
                    firstSearch = true;
                    break;

                case NEXT_SEARCH:
                    firstSearch = false;
                    String str = args.getString(NEXT_SEARCH_ARGS);
                    startIndex = Integer.parseInt(str);
                    Log.d(TAG, "Next Search. Index = " + startIndex);
                    break;
            }
        }
        if(query.isEmpty()){
            Log.w(TAG, "query is empty!");
        }
        Log.d(TAG, "query = " + query);
    }

    @Override
    public Object loadInBackground() {

        URL url;
        String rootUrl = getContext().getString(R.string.search_url_root); //https://www.googleapis.com/customsearch/v1?q=
        String apiKey = getContext().getString(R.string.api_key); //"AIzaSyByrwBdvnGe_ibM5gQo0Z70WscRg-2lUow";
        String cx = getContext().getString(R.string.cx_key);      //"013711002555996113927:-qetakkizte";
        HttpTransport httpTransport = new NetHttpTransport();
        JacksonFactory jacksonFactory = new JacksonFactory();
        Customsearch customsearch = new Customsearch(httpTransport, jacksonFactory, null);

        try {
            Customsearch.Cse.List list = customsearch.cse().list(query);
            list.setKey(apiKey);
            list.setCx(cx);
//            list.setNum(10L);
            list.setSearchType("image");
            if(firstSearch){
//                list.setStart(0L);
            }
            else{
                list.setStart(((Integer)startIndex).longValue());
            }
            Search result = null;
            result = list.execute();
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
