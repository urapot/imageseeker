package com.urapot.imageseeker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.google.api.services.customsearch.model.Query;
import com.google.api.services.customsearch.model.Result;
import com.google.api.services.customsearch.model.Search;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class MainActivity extends SherlockFragmentActivity implements ViewPager.OnPageChangeListener{
    private static final String TAG = "MainActivity";

    public static final String IMAGES_SEARCH_RESULT = "founded images";
    public static final String IMAGES_FAVORITE = "favorites images";
    public static final int IMAGE_LOADER_ID = 1;
    public static final int DB_LOADER_ID = 2;

    public static final String START_BIG_IMAGE_FRAGMENT = "com.urapot.imageseeker.showbigimage";
    public static final String BIG_IMAGE_URL_EXTRA = "big_image_url_extra";
    public static final String BIG_IMAGE_TITLE_EXTRA = "big_image_title_extra";
    public static final String BIG_IMAGE_FAVORITE_EXTRA = "big_image_favorite_extra";

    public static final String INTERACT_WITH_DB = "com.urapot.imageseeker.interact_db";
    public static final String INTERACT_DB_IMAGE_EXTRA = "interact_db_imageinfo_json";
    public static final String INTERACT_DB_FUNCTION = "interact_db_function";
    public static final String INTERACT_DB_ADD = "interact_db_add";
    public static final String INTERACT_DB_DELETE = "interact_db_delete";

    public static final String NEXT_FAVORITE_IMAGES = "next_favorite_images";

    private ViewPager mViewPager;
    private PagerAdapter mPagerAdapter;
    private EditText etSearch;
    private EditText etSearchAB;
    private ImageButton butSearch;

    private ArrayList<ImageInfo> foundedImages =new ArrayList<>();
    private ArrayList<ImageInfo> favoritedImages = new ArrayList<>();
    private Map<String, List<Query>> lastSearchInfo;
    private String lastQuery;
    private BroadcastReceiver mBroadcastReceiverBigImage;
    private BroadcastReceiver mBroadcastReceiverInteractDB;
    private DBAdapter db;
    private int lastFavorId;
    private long favoritesCount;
    private long foundedCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_main);

        db = new DBAdapter(this);
        db.open();
        Cursor c = db.getAllData();
        favoritesCount = c.getCount();

        mBroadcastReceiverBigImage = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "Open big image...");
                String imageUrl = intent.getStringExtra(BIG_IMAGE_URL_EXTRA);
                String imageTitle = intent.getStringExtra(BIG_IMAGE_TITLE_EXTRA);
                boolean favorite = intent.getBooleanExtra(BIG_IMAGE_FAVORITE_EXTRA, false);
                startBigImageDialog(imageUrl, imageTitle, favorite);
            }
        };

        this.registerReceiver(mBroadcastReceiverBigImage, new IntentFilter(START_BIG_IMAGE_FRAGMENT));

        mBroadcastReceiverInteractDB = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "interact with db...");
                ImageInfo imageInfo = ImageInfo.fromIIString(intent.getStringArrayListExtra(INTERACT_DB_IMAGE_EXTRA));
                String function = intent.getStringExtra(INTERACT_DB_FUNCTION);
                switch (function){
                    case INTERACT_DB_ADD:
                        Cursor c = db.findRecords(imageInfo.getLink());
                        if(!c.moveToFirst()) {
                            Log.d(TAG, "added");
                            favoritesCount++;
                            db.addRecord(imageInfo);
                            getFavoriteImages();
                        }
                        break;

                    case INTERACT_DB_DELETE:
                        Cursor c1 = db.findRecords(imageInfo.getLink());
                        if(c1.moveToFirst()) {
                            Log.d(TAG, "deleted");
                            favoritesCount--;
                            db.delRecord(imageInfo.getLink());
                            getFavoriteImages();
                        }
                        break;
                }
            }
        };

        mViewPager = (ViewPager)findViewById(R.id.pager_main);
        mPagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);
        mViewPager.setOnPageChangeListener(this);

        lastFavorId = -1;
        getFavoriteImages();
    }

    private void startBigImageDialog(String imageUrl, String imageTitle, boolean favorite){
        DialogFragment fragmentBigImage = BigImageFragment.newInstance(imageUrl, imageTitle, favorite);
        fragmentBigImage.show(getSupportFragmentManager(), "BigImage");
    }

    public void getFavoriteImages(){
        Log.d(TAG, "getFavoriteImages");
        Bundle dbArgs = new Bundle();
        dbArgs.putString(DBCursorLoader.MODE_ARG, DBCursorLoader.MODE_GET_TEN);
        dbArgs.putInt(DBCursorLoader.FIRST_ID_ARG, lastFavorId);
        getSupportLoaderManager().initLoader(DB_LOADER_ID, dbArgs, new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                Loader<Cursor> loader = null;
                if (id == DB_LOADER_ID) {
                    loader = new DBCursorLoader(getBaseContext(), db, args);
                }
                return loader;
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
                Log.d(TAG, "onLoadFinished DB");
                ImageInfo imageInfo = new ImageInfo();
                if (data.moveToFirst()) {
                    favoritedImages.clear();
                    favoritesCount = 0;
                    int indColId = data.getColumnIndex(DBAdapter.COLUMN_ID);
                    int indColTitle = data.getColumnIndex(DBAdapter.COLUMN_TITLE);
                    int indColLink = data.getColumnIndex(DBAdapter.COLUMN_LINK);
                    int indColHeigth = data.getColumnIndex(DBAdapter.COLUMN_HEIGHT);
                    int indColWidth = data.getColumnIndex(DBAdapter.COLUMN_WIDTH);
                    int indColSize = data.getColumnIndex(DBAdapter.COLUMN_SIZE);
                    int indColThumbLink = data.getColumnIndex(DBAdapter.COLUMN_THUMBLINK);
                    int indColThumbHeigth = data.getColumnIndex(DBAdapter.COLUMN_THUMBHEIGHT);
                    int indColThumbWidth = data.getColumnIndex(DBAdapter.COLUMN_THUMBWIDTH);

                    do {

                        imageInfo = new ImageInfo(data.getString(indColTitle), data.getString(indColLink),
                                data.getInt(indColHeigth), data.getInt(indColWidth), data.getInt(indColSize),
                                data.getString(indColThumbLink), data.getInt(indColThumbHeigth), data.getInt(indColThumbWidth), true);
                        favoritedImages.add(imageInfo);
                        favoritesCount++;
                        lastFavorId = data.getInt(indColId);
                        Log.d(TAG, "last id = " + lastFavorId);
                    } while (data.moveToNext());
                    mPagerAdapter.notifyDataSetChanged();
                } else {
                    Log.d(TAG, "No more records!! id = " + lastFavorId);
                }
            }

            @Override
            public void onLoaderReset(Loader<Cursor> loader) {

            }
        });
        getSupportLoaderManager().getLoader(DB_LOADER_ID).forceLoad();
    }

    public void startSearchImages(String position){
        Log.d(TAG, "startSearchImages");
        final Bundle searchArgs = new Bundle();
        searchArgs.putString(ImageAsyncLoader.SEARCH_POSITION_ARG, position);
        if(position.equals(ImageAsyncLoader.NEXT_SEARCH)){
            searchArgs.putString(ImageAsyncLoader.NEXT_SEARCH_ARGS, ((lastSearchInfo.get("nextPage")).get(0)).getStartIndex().toString());
            searchArgs.putString(ImageAsyncLoader.QUERY_ARG, lastQuery);
        }
        else {
            searchArgs.putString(ImageAsyncLoader.QUERY_ARG, lastQuery);
        }
        getSupportLoaderManager().restartLoader(IMAGE_LOADER_ID, searchArgs, new LoaderManager.LoaderCallbacks<Object>() {
            @Override
            public Loader<Object> onCreateLoader(int id, Bundle args) {
                Loader<Object> loader = null;
                if (id == IMAGE_LOADER_ID) {
                    loader = new ImageAsyncLoader(MainActivity.this, args);
                    ((ImageAsyncLoader) loader).setArgs(args);
                }
                return loader;
            }

            @Override
            public void onLoadFinished(Loader<Object> loader, Object data) {
                Log.d(TAG, "OnLoadFinished SEARCH ");
                if (data != null) {
                    Log.d(TAG, "parsing data...");
                    Search result = (Search) data;
                    lastSearchInfo = result.getQueries();

                    foundedImages.addAll(parseResponce(result));
                    for (ImageInfo i : foundedImages) {
                        Log.d(TAG, i.toString());
                    }
                    foundedCount = lastSearchInfo.get("nextPage").get(0).getTotalResults();
                    Toast.makeText(getApplicationContext(), "Founded " + foundedCount + " images", Toast.LENGTH_SHORT).show();
                    mPagerAdapter.notifyDataSetChanged();
                }
                loader.abandon();
            }

            @Override
            public void onLoaderReset(Loader<Object> loader) {
                ((ImageAsyncLoader) loader).setArgs(searchArgs);
            }
        });
        getSupportLoaderManager().getLoader(IMAGE_LOADER_ID).forceLoad();
    }

    public ArrayList<ImageInfo> getImages(String mode){
        if(mode != null){
            ArrayList<ImageInfo> imageList = new ArrayList<>();

            switch (mode){
                case IMAGES_FAVORITE:
                    if(favoritedImages != null){
                        imageList = favoritedImages;
                    }
                    break;

                case IMAGES_SEARCH_RESULT:
                    if(foundedImages != null){
                        imageList = foundedImages;
                    }
                    break;
            }
            return imageList;
        }
        return null;
    }

    public long getItemCount(String mode){
        if(mode != null){
            long count = 0;

            switch (mode){
                case IMAGES_FAVORITE:
                    count = favoritesCount;
                    Log.d(TAG, "favorCount = " + count);
                    break;

                case IMAGES_SEARCH_RESULT:
                    count = foundedCount;
                    Log.d(TAG, "foundedCount = " + count);
                    break;
            }
            return count;
        }
        return 0;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        Log.d(TAG, "pos = " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private ArrayList<ImageInfo> parseResponce(Search search){
        List<Result> results = search.getItems();
        ArrayList<ImageInfo> imageInfos = new ArrayList<>();

        for(Result r : results){
            ImageInfo imageInfo = new ImageInfo();
            imageInfo.setTitle(r.getTitle());
            imageInfo.setLink(r.getLink());
            imageInfo.setThumbnailLink(r.getImage().getThumbnailLink());
            imageInfos.add(imageInfo);
        }
        return imageInfos;
    }

    @Override
    protected void onPause() {
        unregisterReceiver(mBroadcastReceiverBigImage);
        unregisterReceiver(mBroadcastReceiverInteractDB);
        super.onPause();
    }

    @Override
    protected void onStop() {
//        unregisterReceiver(mBroadcastReceiverBigImage);
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        db.close();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        this.registerReceiver(mBroadcastReceiverBigImage, new IntentFilter(START_BIG_IMAGE_FRAGMENT));
        this.registerReceiver(mBroadcastReceiverInteractDB, new IntentFilter(INTERACT_WITH_DB));
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getSupportMenuInflater().inflate(R.menu.menu_main, menu);

//        menu.getItem(0).setActionView(R.layout.search_layout);
//        menu.getItem(0).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM | MenuItem.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW);
//        menu.findItem(R.id.search_item).setVisible(false);

        MenuItem searchItem = menu.findItem(R.id.search_item);


        etSearchAB = (EditText)searchItem.getActionView().findViewById(R.id.edittext_search);
        etSearchAB.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) {
                    lastQuery = ((EditText)v).getText().toString();
                    Log.d(TAG, "Query entered! " + ((EditText)v).getText());
                    startSearchImages(ImageAsyncLoader.FIRST_SEARCH);
                    return true;
                }
                return false;
            }
        });

        MenuItem menuSearch = menu.findItem(R.id.search_item);
        menuSearch.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem menuItem) {
                Log.d(TAG, "onMenuItemActionExpanded");
                etSearchAB.requestFocus();
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                return false;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem menuItem) {
                Log.d(TAG, "onMenuItemActionCollapse");
                etSearchAB.clearFocus();
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

//    @Override
//    public boolean onPrepareOptionsMenu(Menu menu) {
//        super.onPrepareOptionsMenu(menu);
////        etSearch.setText("");
//        return false;
//    }

/*
    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
//        if()
        return super.onMenuItemSelected(featureId, item);
    }
*/
}
