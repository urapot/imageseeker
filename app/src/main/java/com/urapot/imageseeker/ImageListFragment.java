package com.urapot.imageseeker;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;


public class ImageListFragment extends Fragment implements Updateable, AbsListView.OnScrollListener{
    private static final String TAG = "ImageListFragment";
    private static final String ARG_PARAM1 = "param1";

    ListView lvImages;
    TextView etCount;
    long itemCount;
    long itemCountOld;

    private String entryType;
    private ArrayList<ImageInfo> images;
    ImageListAdapter adapter;
    private boolean loadMore = true;

    public static ImageListFragment newInstance(String param1) {
        ImageListFragment fragment = new ImageListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public String getEntryType() {
        return entryType;
    }

    public ImageListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            entryType = getArguments().getString(ARG_PARAM1);
        }
        loadMore = false;
        Log.d(TAG, "onCreate");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = (View)inflater.inflate(R.layout.fragment_image_list, container, false);
        lvImages = (ListView)rootView.findViewById(R.id.images_listview);
        etCount = (TextView)rootView.findViewById(R.id.items_count);
        lvImages.setOnScrollListener(this);

        images = ((MainActivity)getActivity()).getImages(entryType);
        itemCount = ((MainActivity)getActivity()).getItemCount(entryType);
        if(images != null){
            itemCountOld = images.size();
            Log.d(TAG, "images size = " + images.size() + " " + itemCountOld);
            if(images.size() != 0){
                adapter = new ImageListAdapter(getActivity(), images);
                lvImages.setAdapter(adapter);
                if(!entryType.equals(MainActivity.IMAGES_FAVORITE)){
                    loadMore = true;
                }
            }
            else
            {
                Log.d(TAG, "list empty");
                loadMore = false;
                String[] empty = new String[]{"Nothing to show here!"};
                ArrayAdapter<String> adapterEmpty = new ArrayAdapter<String>(getActivity(),R.layout.item_empty, R.id.item_empty_text, empty);
                lvImages.setAdapter(adapterEmpty);
                itemCountOld = 0;
                itemCount = 0;
                images.clear();
            }
        }
        etCount.setText("Images: " + itemCountOld + "/" + itemCount);
        return rootView;
    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
        if((totalItemCount != 0) && (totalItemCount != firstVisibleItem)){
            if (loadMore) {
                if ((firstVisibleItem + visibleItemCount) > (totalItemCount - 1)) {
                    Log.d(TAG, entryType + " NEED MOAR IMAGES!!11");
                    ((MainActivity) getActivity()).startSearchImages(ImageAsyncLoader.NEXT_SEARCH);
                }
            }
        }
    }

    @Override
    public void update() {
        int top;
        itemCount = ((MainActivity) getActivity()).getItemCount(getEntryType());
        Log.d(TAG, "Updating fragment: " + getEntryType());
        Log.d(TAG, " " + (images.size() - 1) + " " + itemCountOld + " " + itemCount);
        if(itemCountOld < itemCount) {
            images = ((MainActivity) getActivity()).getImages(getEntryType());
            Log.d(TAG, "images: " + images.size());
            if (adapter != null) {

                Log.d(TAG, "NOT NULL");
                top = lvImages.getFirstVisiblePosition();
                adapter = new ImageListAdapter(getActivity(), images);
                lvImages.setAdapter(adapter);
                lvImages.setSelectionFromTop(top, 0);
                itemCountOld = images.size();
                if(!entryType.equals(MainActivity.IMAGES_FAVORITE)){
                    loadMore = true;
                }
            } else {
                Log.d(TAG, "NULL");
                if (images != null) {
                    Log.d(TAG, "images size = " + images.size());
                    itemCountOld = images.size();
                    if (images.size() != 0) {
                        adapter = new ImageListAdapter(getActivity(), images);
                        lvImages.setAdapter(adapter);
                        if(!entryType.equals(MainActivity.IMAGES_FAVORITE)){
                            loadMore = true;
                        }
                    } else {
                        Log.d(TAG, "list empty");
                        String[] empty = new String[]{"Nothing to show here!"};
                        ArrayAdapter<String> adapterEmpty = new ArrayAdapter<String>(getActivity(), R.layout.item_empty, R.id.item_empty_text, empty);
                        lvImages.setAdapter(adapterEmpty);
                        itemCountOld = 0;
                        itemCount = 0;
                        loadMore = false;
                    }
                }
            }
            etCount.setText("Images: " + itemCountOld + "/" + itemCount);
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
    }
}
